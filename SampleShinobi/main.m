//
//  main.m
//  SampleShinobi
//
//  Created by Trung Tran on 4/20/16.
//  Copyright © 2016 Tasumi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
