//
//  TradingViewController.h
//  QuoineExiOS
//
//  Created by Trung Tran on 2/25/16.
//  Copyright © 2016 Trung Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    Freq1Min = 0, Freq5Min, Freq15Min, Freq30Min, Freq1Hour, Freq6Hour,
    Freq12Hour, Freq1D, Freq3D, Freq1W
} FrequentInterval;

@interface TradingViewController : UIViewController

@end
