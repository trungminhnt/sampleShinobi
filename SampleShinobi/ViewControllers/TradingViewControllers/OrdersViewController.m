//
//  OrdersViewController.m
//  SampleShinobi
//
//  Created by Trung Tran on 4/21/16.
//  Copyright © 2016 Tasumi. All rights reserved.
//

#import "OrdersViewController.h"
#import "SWRevealViewController.h"

@interface OrdersViewController ()

@end

@implementation OrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)menuButtonTouchUp:(id)sender
{
    [self.revealViewController rightRevealToggleAnimated:true];
}

@end
