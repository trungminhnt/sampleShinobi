//
//  TradingChartTableViewCell.m
//  QuoineExiOS
//
//  Created by Trung Tran on 2/25/16.
//  Copyright © 2016 Trung Tran. All rights reserved.
//

#import "TradingChartTableViewCell.h"
#import "MACDChartDataSource.h"
#import <Pusher/Pusher.h>
#import "UIColor+TTAdditions.h"
#import "Constants.h"
#import "UIColor+BTUtils.h"

// Chart
#import <ShinobiCharts/ShinobiCharts.h>
#import "StockChartValueAnnotationManager.h"
#import "CandleSticksChartDataSource.h"
#import "BaseChartDataSource.h"
#import "NSArray+StockChartUtils.h"
#import "StockChartRangeChartDataSource.h"
#import "ProductManager.h"

#define kReloadMACDLastVisible   @"ReloadMACDLastVisible"

@interface TradingChartTableViewCell()<SChartDelegate>

@property (weak, nonatomic) IBOutlet UIView *loadingMACDView;
@property (weak, nonatomic) IBOutlet UIView *loadingChartView;
@property (weak, nonatomic) IBOutlet UIView      *candleView;
@property (weak, nonatomic) IBOutlet UIView      *macdView;
@property (weak, nonatomic) IBOutlet UIView      *dateView;
@property (weak, nonatomic) IBOutlet UIView      *laddersView;
@property (weak, nonatomic) IBOutlet UIButton    *periodButton;
@property (weak, nonatomic) IBOutlet UILabel     *openLabel;
@property (weak, nonatomic) IBOutlet UILabel     *highLabel;
@property (weak, nonatomic) IBOutlet UILabel     *lowLabel;
@property (weak, nonatomic) IBOutlet UILabel     *closeLabel;
@property (weak, nonatomic) IBOutlet UILabel     *macdLabel;
@property (weak, nonatomic) IBOutlet UITableView *sellTableView;
@property (weak, nonatomic) IBOutlet UITableView *buyTableView;
@property (weak, nonatomic) IBOutlet UILabel *diffLaddersLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthConstraintLaddersView;

// Chart
@property (strong, nonatomic) ShinobiChart *candleChart;
@property (strong, nonatomic) ShinobiChart *macdChart;
@property (strong, nonatomic) CandleSticksChartDataSource *candleDataSource;
@property (strong, nonatomic) MACDChartDataSource *macdDataSource;

@property (strong, nonatomic) StockChartValueAnnotationManager *valueAnnotationManagerCandleChart;
@property (strong, nonatomic) StockChartValueAnnotationManager *valueAnnotationManagerMACDChart;
@property (strong, nonatomic) StockChartValueAnnotationManager *signalAnnotationManagerMACDChart;
@property (nonatomic) float minXAxisRange; // Limit the x axis so it has a minimum range of a week
@property (assign, atomic) BOOL isFirstLoadCandleChart;
@property (assign, atomic) BOOL isFirstLoadMACDChart;
@property (assign, atomic) BOOL enableActivityLoading;

@end

@implementation TradingChartTableViewCell

- (void)awakeFromNib
{
//    self.period = Freq1Hour;
    
    // Setup tableview
    self.sellTableView.scrollEnabled = false;
    self.buyTableView.scrollEnabled = false;
    
    [self.sellTableView registerNib:[UINib nibWithNibName:@"PriceLadderTableViewCell" bundle:nil] forCellReuseIdentifier:@"PriceLadderTableViewCell"];
    [self.buyTableView registerNib:[UINib nibWithNibName:@"PriceLadderTableViewCell" bundle:nil] forCellReuseIdentifier:@"PriceLadderTableViewCell"];
    
    // NotificationCenter
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadMACDLastVisibleValue)
                                                 name:kReloadMACDLastVisible
                                               object:nil];
    // Initial charts
    [self intitalCharts];
}

#pragma mark - Accessories

- (void)setPeriod:(FrequentInterval)period
{
    NSInteger step = 15;
    _period = period;
    switch (period) {
        case Freq1Min: {
            self.minXAxisRange = 60 * step;
        }
            break;
        case Freq5Min: {
            self.minXAxisRange = 60 * 5 * step;
        }
            break;
        case Freq15Min: {
            self.minXAxisRange = 60 * 15 * step;
        }
            break;
        case Freq30Min: {
            self.minXAxisRange = 60 * 30 * step;
        }
            break;
        case Freq1Hour: {
            self.minXAxisRange = 60 * 60 * step;
        }
            break;
        case Freq6Hour: {
            self.minXAxisRange = 60 * 60 * 6 * step;
        }
            break;
        case Freq12Hour: {
            self.minXAxisRange = 60 * 60 * 12 * step;
        }
            break;
        case Freq1D: {
            self.minXAxisRange = 60 * 60 * 24 * step;
        }
            break;
        case Freq3D: {
            self.minXAxisRange = 60 * 60 * 24 * 3 * step;
        }
            break;
        case Freq1W: {
            self.minXAxisRange = 60 * 60 * 24 * 7 * step;
        }
            break;
        default: {
            self.minXAxisRange = 60 * 60 * 24 * step;
        }
            break;
    }
    
    // Set period title button
    [self.periodButton setTitle:kFreqTimeItems[_period] forState:UIControlStateNormal];
    
    // Cache period time
    [ProductManager sharedInstance].periodTime = _period;

    // Loading view
    self.loadingChartView.alpha = [CandleSticksChartDataSource sharedInstance].data.hasData ? 0 : 1;
    self.loadingMACDView.alpha  = [MACDChartDataSource sharedInstance].data.hasData ? 0 : 1;
    
    // Reload charts
    [self intitalCharts];
}

//- (void)reloadCharts
//{
//    [self intitalCharts];
//    [self.candleChart reloadData];
//    [self.macdChart reloadData];
//    [self.candleChart redrawChart];
//    [self.macdChart redrawChart];
//}
//

- (void)forceShowLoadingViewAndResetData:(BOOL)flag
{
    [self.activityLoadingView startAnimating];
    self.loadingChartView.alpha = 1;
    self.loadingMACDView.alpha  = 1;
    
    if (flag) {
        [self reset];
    }
}

- (void)reset
{
    self.diffLaddersLabel.text = @"0.0";
    self.openLabel.text = @"0";
    self.highLabel.text = @"0";
    self.lowLabel.text = @"0";
    self.closeLabel.text = @"0";
    [self.sellTableView reloadData];
    [self.buyTableView reloadData];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self.candleChart removeFromSuperview];
    [self.macdChart removeFromSuperview];
    self.candleChart = nil;
    self.macdChart = nil;
}

#pragma mark - Setup Charts

- (void)intitalCharts
{
    if (!self.candleChart) {
        if ([CandleSticksChartDataSource sharedInstance].data.hasData) {
            [self setupChartView:self.candleView withDataSource:[CandleSticksChartDataSource sharedInstance]];
        }
    }
    else {
        if ([CandleSticksChartDataSource sharedInstance].data.hasData) {
            [self.candleChart reloadData];
            [self.candleChart redrawChart];
        }
    }
    
    if (!self.macdChart) {
        if ([MACDChartDataSource sharedInstance].data.hasData) {
            [self setupChartView:self.macdView withDataSource:[MACDChartDataSource sharedInstance]];
        }
    }
    else {
        if ([MACDChartDataSource sharedInstance].data.hasData) {
            [self.macdChart reloadData];
            [self.macdChart redrawChart];
        }
    }
}

- (void)setupChartView:(UIView *)view withDataSource:(BaseChartDataSource *)dataSource
{
    ShinobiChart *chart = nil;
    if ([dataSource isKindOfClass:[CandleSticksChartDataSource class]]) {
        self.candleDataSource = (CandleSticksChartDataSource *)dataSource;
        chart = [self createChartWithBounds:view.bounds
                                 dataSource:self.candleDataSource];
        self.isFirstLoadCandleChart = false;
    } else {
        self.macdDataSource = (MACDChartDataSource *)dataSource;
        chart = [self createChartWithBounds:view.bounds
                                 dataSource:self.macdDataSource];
        self.isFirstLoadMACDChart = false;
    }
    
    // Create the range chart and annotation
    chart.clipsToBounds = false;
    
    // Disable chart gesture
    chart.gestureManager.doubleTapResetsZoom = false;
    chart.gestureManager.doubleTapEnabled    = false;
    
    if ([dataSource isKindOfClass:[CandleSticksChartDataSource class]]) {
        // Create the series marker (it's added to the view in viewDidAppear)
        self.valueAnnotationManagerCandleChart = [[StockChartValueAnnotationManager alloc] initWithChart:chart
                                                                                              datasource:dataSource
                                                                                             seriesIndex:0
                                                                                            rightPadding:self.widthPercentView];
        [self.valueAnnotationManagerCandleChart updateValueAnnotationForXAxisRange:chart.xAxis.defaultRange
                                                                        yAxisRange:chart.yAxis.defaultRange
                                                                            redraw:false];
        self.valueAnnotationManagerCandleChart.textColor = [UIColor chartRedColor];
        self.candleChart = chart;
    } else {
        self.valueAnnotationManagerMACDChart = [[StockChartValueAnnotationManager alloc] initWithChart:chart
                                                                                            datasource:dataSource
                                                                                           seriesIndex:0
                                                                                          rightPadding:0];
        [self.valueAnnotationManagerMACDChart updateValueAnnotationForXAxisRange:chart.xAxis.defaultRange
                                                                      yAxisRange:chart.yAxis.defaultRange
                                                                          redraw:false];
        self.signalAnnotationManagerMACDChart = [[StockChartValueAnnotationManager alloc] initWithChart:chart
                                                                                             datasource:dataSource
                                                                                            seriesIndex:1
                                                                                           rightPadding:0];
        self.signalAnnotationManagerMACDChart.textColor   = [UIColor chartSignalLineColor];
        self.valueAnnotationManagerMACDChart.textColor    = [UIColor chartMACDLineColor];
        self.signalAnnotationManagerMACDChart.disableLine = true;
        self.valueAnnotationManagerMACDChart.disableLine  = true;
        self.macdChart = chart;
    }
    
    [view addSubview:chart];
    
    // Add a chart delegate
    chart.delegate = self;
    chart.licenseKey = [Constants shinobiControlsKey];
}

- (ShinobiChart*)createChartWithBounds:(CGRect)bounds dataSource:(id<SChartDatasource>)datasource
{
    ShinobiChart *chart = [[ShinobiChart alloc] initWithFrame:bounds];
    chart.backgroundColor = [UIColor chartBackgroundColor];
    chart.plotAreaBackgroundColor = [UIColor chartBackgroundColor];
    
    // As the chart is a UIView, set its resizing mask to allow it to automatically resize when screen orientation changes.
    [chart setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    chart.rotatesOnDeviceRotation = false;
    
    // Give the chart the data source
    chart.datasource = datasource;
    
    // Create a date time axis to use as the x axis.
    SChartDateTimeAxis *xAxis = [SChartDateTimeAxis new];
    xAxis.enableGesturePanning  = true;
    xAxis.enableGestureZooming  = true;
    xAxis.enableMomentumPanning = true;
    xAxis.enableMomentumZooming = false;
    
    // Disable border xAxis
    xAxis.style.lineWidth = @0;
    
    // Disable labels and ticks xAxis
    xAxis.style.majorTickStyle.showLabels = false;
    xAxis.style.majorTickStyle.showTicks  = true;
    xAxis.style.majorGridLineStyle.showMajorGridLines = true;
    xAxis.style.majorGridLineStyle.lineWidth = @0.5;
    xAxis.style.majorGridLineStyle.lineColor = [[UIColor colorWithHexString:@"#EBEBEB"] colorWithAlphaComponent:0.7];
    
    //Make some space at the axis limits to prevent clipping of the datapoints
    xAxis.rangePaddingHigh = @(1);
    xAxis.rangePaddingLow  = @(1);
    xAxis.tickLabelClippingModeHigh = SChartTickLabelClippingModeTicksAndLabelsPersist;
    chart.xAxis = xAxis;
    
    // Create a number axis to use as the y axis.
    SChartNumberAxis *yAxis = [SChartNumberAxis new];
    yAxis.enableGesturePanning  = false;
    yAxis.enableGestureZooming  = false;
    yAxis.enableMomentumPanning = false;
    yAxis.enableMomentumZooming = false;
    yAxis.axisPosition = SChartAxisPositionReverse;
    yAxis.width = @40;
    
    // Disable border yAxis
    yAxis.style.lineWidth = @0;
    
    // Disable labels and ticks yAxis
    yAxis.style.majorTickStyle.showLabels = false;
    yAxis.style.majorTickStyle.labelColor = [UIColor clearColor];
    yAxis.style.majorTickStyle.showTicks  = true;
    yAxis.style.majorGridLineStyle.showMajorGridLines = true;
    yAxis.style.majorGridLineStyle.lineWidth = @0.5;
    yAxis.style.majorGridLineStyle.lineColor = [[UIColor colorWithHexString:@"#EBEBEB"] colorWithAlphaComponent:0.7];
    chart.yAxis = yAxis;
    
    // Load data in background
    chart.loadDataInBackground = true;
    
    return chart;
}

#pragma mark - Update Charts

- (void)updateYAxisInChart:(ShinobiChart *)chart andDataSource:(BaseChartDataSource *)dataSource withRange:(SChartRange *)range
{
    StockChartData *chartData = dataSource.data;
    NSDate *startValue = [NSDate dateWithTimeIntervalSince1970:[range.minimum doubleValue]];
    NSDate *endValue = [NSDate dateWithTimeIntervalSince1970:[range.maximum doubleValue]];
    
    // Find the index of the dates nearest the start and end values
    NSUInteger lowerIndex;
    @try {
        lowerIndex = [chartData.dates indexOfBiggestObjectSmallerThan:startValue
                                                        inSortedRange:NSMakeRange(0, chartData.dates.count)];
    } @catch (NSException *e) {
        lowerIndex = 0;
    }
    
    NSUInteger upperIndex;
    @try {
        upperIndex = [chartData.dates indexOfSmallestObjectBiggerThan:endValue
                                                        inSortedRange:NSMakeRange(lowerIndex, chartData.dates.count - lowerIndex)];
    } @catch (NSException *e) {
        upperIndex = [chartData.dates count] - 1;
    }

    if (chart == self.candleChart) {
        // Candle chart
        
        double min = [[chartData yMinInRangeFromIndex:lowerIndex toIndex:upperIndex] doubleValue];
        double max = [[chartData yMaxInRangeFromIndex:lowerIndex toIndex:upperIndex] doubleValue];
        
        // Add some padding
        double padding = 0.1 * (max - min);
        double minValue = (min - padding > 0) ? min - padding : 0;
        double maxValue = max + padding;
        if ((int)minValue  == (int)maxValue && minValue > 10) {
            minValue = maxValue - 5;
            maxValue = maxValue + 5;
        }
        SChartRange * newRange = [[SChartRange alloc] initWithMinimum:@(minValue) andMaximum:@(maxValue)];
        [self.candleChart.yAxis setRange:newRange];
        
        //        // Set highest and lowest price on yAxis column
        //        self.highestPriceLabel.text = [NSString stringWithFormat:@"%.2f", chart.yAxis.range.maximum.floatValue];
        //        self.lowestPriceLabel.text  = [NSString stringWithFormat:@"%.2f", chart.yAxis.range.minimum.floatValue];

        // Set open and close price on candlesticks chart
        SChartMultiYDataPoint *candlestickPoint = [self.candleDataSource sChart:self.candleChart dataPointAtIndex:upperIndex forSeriesAtIndex:0];
        [self setLabelsLastTicker:candlestickPoint];
    } else {
        // MACD chart
        double min = [[chartData yMinInRangeFromIndexOfMACDArray:lowerIndex toIndex:upperIndex] doubleValue];
        double max = [[chartData yMaxInRangeFromIndexOfMACDArray:lowerIndex toIndex:upperIndex] doubleValue];
        
        // Add some padding
        double padding = 0.2 * (max - min);
        double minValue = min - padding;
        double maxValue = max + padding;
        SChartRange * newRange = [[SChartRange alloc] initWithMinimum:@(minValue) andMaximum:@(maxValue)];
        [self.macdChart.yAxis setRange:newRange];
    }
}

- (void)setLabelsLastTicker:(SChartMultiYDataPoint *)candlestickPoint
{
    NSString *format = [[@"%." stringByAppendingFormat:@"%tu", 2] stringByAppendingString:@"f"];

    NSString *priceStr = [NSString stringWithFormat:format, [candlestickPoint.yValues[@"Open"] doubleValue]];
    self.openLabel.text  = priceStr;
    priceStr = [NSString stringWithFormat:format, [candlestickPoint.yValues[@"High"] doubleValue]];
    self.highLabel.text  = priceStr;
    priceStr = [NSString stringWithFormat:format, [candlestickPoint.yValues[@"Low"] doubleValue]];
    self.lowLabel.text   = priceStr;
    priceStr = [NSString stringWithFormat:format, [candlestickPoint.yValues[@"Close"] doubleValue]];
    self.closeLabel.text = priceStr;
}

#pragma mark - SChartDelegate

- (void)sChartIsPanning:(ShinobiChart *)chart withChartMovementInformation:(const SChartMovementInformation *)information
{
    // Calculate new yAxis from min and max xAxis
    // Get index of datasource through min and max xAxis
    BaseChartDataSource *dataSource = (chart == self.candleChart) ? self.candleDataSource : self.macdDataSource;
    [self updateYAxisInChart:chart andDataSource:dataSource withRange:chart.xAxis.range];
    if (chart == self.candleChart) {
        [self.valueAnnotationManagerCandleChart updateValueAnnotationForXAxisRange:chart.xAxis.range
                                                                        yAxisRange:chart.yAxis.range];
    } else {
        [self.valueAnnotationManagerMACDChart updateValueAnnotationForXAxisRange:chart.xAxis.range
                                                                      yAxisRange:chart.yAxis.range];
        [self.signalAnnotationManagerMACDChart updateValueAnnotationForXAxisRange:chart.xAxis.range
                                                                       yAxisRange:chart.yAxis.range];
    }
    
    // Update the chart
    if (chart == self.candleChart) {
        [self.macdChart redrawChart];
        [self.macdChart.xAxis setRange:chart.xAxis.range withAnimation:false];
        [self updateYAxisInChart:self.macdChart andDataSource:self.macdDataSource withRange:chart.xAxis.range];
    } else {
        [self.candleChart redrawChart];
        [self.candleChart.xAxis setRange:chart.xAxis.range withAnimation:false];
        [self updateYAxisInChart:self.candleChart andDataSource:self.candleDataSource withRange:chart.xAxis.range];
    }
}

-(void)sChartIsZooming:(ShinobiChart *)chart withChartMovementInformation:(const SChartMovementInformation *)information
{
    // Only redraw the chart after the second annotation has updated
    if (chart == self.candleChart) {
        [self.valueAnnotationManagerCandleChart updateValueAnnotationForXAxisRange:chart.xAxis.range
                                                                        yAxisRange:chart.yAxis.range];
    } else {
        [self.valueAnnotationManagerMACDChart updateValueAnnotationForXAxisRange:chart.xAxis.range
                                                                      yAxisRange:chart.yAxis.range];
        [self.signalAnnotationManagerMACDChart updateValueAnnotationForXAxisRange:chart.xAxis.range
                                                                       yAxisRange:chart.yAxis.range];
    }
}

- (void)sChartRenderFinished:(ShinobiChart *)chart
{
    // It is the main chart which has finished rendering
    // Update the yAxis width on the range chart to match that in the mainChart
    // Add a background view (gray box) for the x-axis
    // Box should be the width of the plot area without axes
    CGFloat boxWidth = [chart getPlotAreaFrame].size.width;
    CGFloat xPos = [chart getPlotAreaFrame].origin.x;
    
    for (SChartAxis *axis in chart.allYAxes) {
        if (axis.axisPosition == SChartAxisPositionNormal) {
            xPos -= [axis.style.lineWidth doubleValue];
        }
        boxWidth += [axis.style.lineWidth doubleValue];
    }
    
    // Update the annotations (without triggering another redraw)
    if (chart == self.candleChart) {
        [self.valueAnnotationManagerCandleChart updateValueAnnotationForXAxisRange:chart.xAxis.range
                                                             yAxisRange:chart.yAxis.range
                                                                 redraw:NO];
    } else {
        [self.valueAnnotationManagerMACDChart updateValueAnnotationForXAxisRange:chart.xAxis.range
                                                             yAxisRange:chart.yAxis.range
                                                                 redraw:NO];
        [self.signalAnnotationManagerMACDChart updateValueAnnotationForXAxisRange:chart.xAxis.range
                                                              yAxisRange:chart.yAxis.range
                                                                  redraw:NO];
    }
}

- (void)sChartWillStartLoadingData:(ShinobiChart *)chart
{
    UIActivityIndicatorView *loadingIndicator = (UIActivityIndicatorView *)chart.loadingIndicator;
    loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    loadingIndicator.color = self.enableActivityLoading ? [UIColor chartBlueColor] : [UIColor clearColor];
}

- (void)sChartDidFinishLoadingData:(ShinobiChart *)chart
{
    // Show axes on both charts
    for (SChartAxis *axis in [chart.allAxes arrayByAddingObjectsFromArray:chart.allAxes]) {
        axis.style.lineColor = [UIColor textBeforeDecimalPointColor];
    }
    
    if (chart == self.candleChart && (!self.isFirstLoadCandleChart)) {
        self.isFirstLoadCandleChart = true;
    }
    if (chart == self.candleChart && (!self.isFirstLoadMACDChart)) {
        self.isFirstLoadMACDChart = true;
    }
    
    [self updateChartInFirstLoad:chart];
}

- (void)updateChartInFirstLoad:(ShinobiChart *)chart
{
    BaseChartDataSource *dataSource = chart == self.candleChart ? self.candleDataSource : self.macdDataSource;
    NSInteger numberPoints = [dataSource sChart:chart numberOfDataPointsForSeriesAtIndex:0];
    if (numberPoints >= 30) {
        // xAxis range
        SChartDataPoint *minDP  = [dataSource sChart:chart dataPointAtIndex:(numberPoints - 30) forSeriesAtIndex:0];
        SChartDataPoint *maxDP  = [dataSource sChart:chart dataPointAtIndex:(numberPoints - 1) forSeriesAtIndex:0];
        
        // Range to update annotation view
        SChartRange * range = [[SChartRange alloc] initWithMinimum:minDP.xValue andMaximum:maxDP.xValue];
        
        // Time interval range -> to create a SChartRange with timeinterval
        SChartRange * rangeUpate = [[SChartRange alloc] initWithMinimum:[NSNumber numberWithDouble:[minDP.xValue timeIntervalSince1970]]
                                                             andMaximum:[NSNumber numberWithDouble:[maxDP.xValue timeIntervalSince1970]]];
        
        
        // Update yAxis
        [self updateYAxisInChart:chart andDataSource:dataSource withRange:rangeUpate];
        
        // Update xAxis
        SChartRange * newRange = [[SChartRange alloc] initWithMinimum:range.minimum andMaximum:range.maximum];
        [chart.xAxis setRange:newRange];
        
        // Update annotations view
        if (chart == self.candleChart) {
            [self.valueAnnotationManagerCandleChart updateValueAnnotationForXAxisRange:range
                                                                            yAxisRange:chart.yAxis.range];
        } else {
            [self.valueAnnotationManagerMACDChart updateValueAnnotationForXAxisRange:range
                                                                          yAxisRange:chart.yAxis.range];
            [self.signalAnnotationManagerMACDChart updateValueAnnotationForXAxisRange:range
                                                                           yAxisRange:chart.yAxis.range];
        }
    }
}

- (void)sChartIsZooming:(ShinobiChart *)chart
{
    NSNumber *xAxisSpan = chart.xAxis.range.span;
    if ([xAxisSpan intValue] < self.minXAxisRange || [xAxisSpan intValue] > self.minXAxisRange * 5) {
        NSNumber *min = chart.xAxis.range.minimum;
        int center = [min floatValue] + ([xAxisSpan floatValue] / 2);
        
        float adjust = ([xAxisSpan floatValue] > self.minXAxisRange * 5) ? self.minXAxisRange * 5 : self.minXAxisRange;
        NSNumber *newMin = @(center - (adjust / 2));
        NSNumber *newMax = @(center + (adjust / 2));
        
        [chart.xAxis setRange:[[SChartRange alloc] initWithMinimum:newMin andMaximum:newMax]];
        [chart redrawChart];
    }
}

#pragma mark - Button Actions

- (IBAction)periodButtonTouchUp:(id)sender
{
    if (self.periodButtonTouchUp) self.periodButtonTouchUp();
}

#pragma mark - Notification Center

- (void)reloadMACDLastVisibleValue
{
    NSString *strValue = [NSString stringWithFormat:@"%.6f", [MACDChartDataSource sharedInstance].lastVisibleValue];
    self.macdLabel.text = strValue;
}

#pragma mark - Size

+ (CGFloat)height
{
    return SCREEN_HEIGHT / 2;
}

#pragma mark - Dealloc

- (void)dealloc
{
    [_candleChart removeFromSuperview];
    [_macdChart removeFromSuperview];
    self.macdChart = nil;
    self.candleChart = nil;

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    _candleDataSource = nil;
    _macdDataSource = nil;
    _valueAnnotationManagerCandleChart = nil;
    _valueAnnotationManagerMACDChart = nil;
    _signalAnnotationManagerMACDChart = nil;
}

@end
