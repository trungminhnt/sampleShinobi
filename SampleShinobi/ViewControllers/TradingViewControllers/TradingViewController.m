//
//  TradingViewController.m
//  QuoineExiOS
//
//  Created by Trung Tran on 2/25/16.
//  Copyright © 2016 Trung Tran. All rights reserved.
//

#import "TradingViewController.h"
#import "TradingChartTableViewCell.h"
#import "ProductManager.h"
#import "TradingChartTableViewCell.h"
#import "RMUniversalAlert.h"
#import "APIHelpers.h"
#import "SWRevealViewController.h"
#import "StockChartData.h"

@interface TradingViewController ()

@property (nonatomic, assign) FrequentInterval period;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation TradingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.period = [ProductManager sharedInstance].periodTime;
}

- (void)viewDidAppear:(BOOL)animated
{
    // Fetch data
    [self fetchChartData];
}

- (void)viewDidDisappear:(BOOL)animated
{
    TradingChartTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell forceShowLoadingViewAndResetData:true];
}

#pragma mark -

- (IBAction)menuButtonTouchUp:(id)sender
{
    [self.revealViewController rightRevealToggleAnimated:true];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"TradingChartTableViewCell";
    TradingChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.period = self.period;
    cell.periodButtonTouchUp = ^() {
        [self showOptionsTimeInterval];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TradingChartTableViewCell.height;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

#pragma mark - Fetching data

/**
 *  Fetch chart data with currency and period
 */
- (void)fetchChartData
{
    [APIHelpers getTickersWithProductCode:@"btcusd.json"
                               onComplete:^(NSDictionary *dict) {
                                   [[StockChartData sharedInstance] loadChartDataFromBigJson:dict
                                                                                      period:self.period
                                                                                  onComplete:^{
                                                                                      [self.tableView reloadData];
                                                                                  }];
                               } onFailure:^(NSString *message) {
                               }];
    
}

#pragma mark - Period

- (void)showOptionsTimeInterval
{
    NSMutableArray *texts = [[NSMutableArray alloc] init];
    for (int i = 0; i < kFreqTimeItems.count; i++) {
        if (i != self.period) {
            [texts addObject:kFreqTimeItems[i]];
        }
    }
    
    [RMUniversalAlert showActionSheetInViewController:self
                                            withTitle:@"Time"
                                              message:nil
                                    cancelButtonTitle:@"Cancel"
                               destructiveButtonTitle:nil
                                    otherButtonTitles:texts
                   popoverPresentationControllerBlock:nil
                                             tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
                                                 if (buttonIndex != alert.cancelButtonIndex) {
                                                     self.period = [self freqTimeIntervalWithText:texts[buttonIndex - 2]];
                                                     [ProductManager sharedInstance].periodTime = self.period;
                                                     
                                                     TradingChartTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                                                     [cell forceShowLoadingViewAndResetData:false];
                                                     
                                                     // Request data
                                                     [self fetchChartData];
                                                 }
                                             }];
}

- (FrequentInterval)freqTimeIntervalWithText:(NSString *)input
{
    for (int i = 0; i < kFreqTimeItems.count; i++) {
        NSString *text = kFreqTimeItems[i];
        if ([text isEqualToString:input]) {
            return i;
        }
    }
    return 0;
}

#pragma mark - Dealloc

- (void)dealloc
{
    [self.tableView setDelegate:nil];
    [self.tableView setDataSource:nil];
    [self.tableView removeFromSuperview];
    self.tableView = nil;
}

@end
