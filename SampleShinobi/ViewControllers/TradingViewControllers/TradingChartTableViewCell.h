//
//  TradingChartTableViewCell.h
//  QuoineExiOS
//
//  Created by Trung Tran on 2/25/16.
//  Copyright © 2016 Trung Tran. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TradingViewController.h"

#define kFreqTimeItems @[@"1M", @"5M", @"15M", @"30M", @"1H", @"6H", @"12H", @"1D", @"3D", @"1W"]

@interface TradingChartTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityLoadingView;
@property (nonatomic, assign) float widthPercentView;

@property (nonatomic, assign) FrequentInterval period;
@property (nonatomic, copy) void (^periodButtonTouchUp)();

+ (CGFloat)height;
- (void)forceShowLoadingViewAndResetData:(BOOL)flag;

@end
