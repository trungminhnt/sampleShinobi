//
//  LeftMenuViewController.m
//  QuoineExiOS
//
//  Created by Trung Tran on 12/24/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "AppDelegate.h"

@interface LeftMenuViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation LeftMenuViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.showsVerticalScrollIndicator = false;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = (0 == indexPath.row) ? @"Chart" : @"Order";
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == TradingViewCell) {
        [self performSegueWithIdentifier:@"ChartSegue" sender:nil];
    }
    if (indexPath.row == OrdersCell) {
        [self performSegueWithIdentifier:@"OrderSegue" sender:nil];
    }
}

@end
