//
//  LeftMenuViewController.h
//  QuoineExiOS
//
//  Created by Trung Tran on 12/24/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    TradingViewCell = 0,
    OrdersCell
} SettingCells;

@interface LeftMenuViewController : UIViewController

@end
