Pod::Spec.new do |s|
  s.name         = "ShinobiCharts"
  s.version      = "2.8.5"
  s.summary      = "ShinobiCharts"
  s.license      = 'Private'
  s.homepage     = "http://www.shinobicontrols.com"
  s.author       = { "Trung Tran" => "trungnmtran@gmail.com" }
  s.vendored_frameworks = 'ShinobiCharts/ShinobiCharts.framework'
  s.frameworks   = 'QuartzCore', 'OpenGLES', 'CoreText', 'Security'
  s.library      = 'c++'
end
