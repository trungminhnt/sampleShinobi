//
//  Constants.h
//  ATViOSApp
//
//  Created by Trung Tran on 10/5/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

// Screen
#define SCREEN_WIDTH    ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT   ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

@interface Constants : NSObject

#pragma mark - ShinobiControls

+ (NSString *)shinobiControlsKey;

@end
