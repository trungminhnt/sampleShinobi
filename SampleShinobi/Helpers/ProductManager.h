//
//  ProductManager.h
//  QuoineExiOS
//
//  Created by Trung Tran on 12/15/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TradingViewController.h"

@interface ProductManager : NSObject

@property (nonatomic, assign) FrequentInterval periodTime;// Global period of the chart
+ (instancetype)sharedInstance;

@end
