#import <Foundation/Foundation.h>
#import <ShinobiCharts/ShinobiCharts.h>
#import "StockChartData.h"

@interface StockChartRangeChartDataSource : NSObject<SChartDatasource>

@property (nonatomic, strong) StockChartData *chartData;

@end
