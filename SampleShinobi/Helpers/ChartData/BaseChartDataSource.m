//
//  BaseChartDataSource.m
//  QuoineExiOS
//
//  Created by Trung Tran on 12/12/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import "BaseChartDataSource.h"

@implementation BaseChartDataSource

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.data = [StockChartData sharedInstance];
    }
    return self;
}

- (void)dealloc
{
    // implement -dealloc & remove abort() when refactoring for
    // non-singleton use.
    abort();
    _data = nil;
}

#pragma mark Datasource Protocol Functions

// Returns the number of series in the specified chart
- (NSInteger)numberOfSeriesInSChart:(ShinobiChart *)chart
{
    return 0;
}

// Returns the series at the specified index for a given chart
- (SChartSeries *)sChart:(ShinobiChart *)chart seriesAtIndex:(NSInteger)index
{
    return nil;
}

// Returns the number of points for a specific series in the specified chart
- (NSInteger)sChart:(ShinobiChart *)chart numberOfDataPointsForSeriesAtIndex:(NSInteger)seriesIndex
{
    return [self.data numberOfDataPoints];
}

- (SChartAxis*)sChart:(ShinobiChart *)chart yAxisForSeriesAtIndex:(NSInteger)index
{
    NSArray *allYAxes = [chart allYAxes];
    return allYAxes[0];
}

// Returns the data point at the specified index for the given series/chart.
- (id<SChartData>)sChart:(ShinobiChart *)chart dataPointAtIndex:(NSInteger)dataIndex forSeriesAtIndex:(NSInteger)seriesIndex
{
    return nil;
}

#pragma mark - StockChartDatasourceLookup methods

- (id)estimateYValueForXValue:(id)xValue forSeriesAtIndex:(NSUInteger)idx
{
    return nil;
}

- (NSDictionary *)getValuesForIndex:(NSInteger)dataPointIndex
{
    // Create dictionary based on OHLC data
    SChartMultiYDataPoint *ohlcDp = [self sChart:nil dataPointAtIndex:dataPointIndex forSeriesAtIndex:0];
    NSMutableDictionary *values = [ohlcDp.yValues mutableCopy];
    values[@"Date"] = ohlcDp.xValue;
    
    // Get volume data and add it in
    SChartDataPoint *volumeDp = [self sChart:nil dataPointAtIndex:dataPointIndex forSeriesAtIndex:1];
    values[@"Volume"] = volumeDp.yValue;
    
    return [values copy];
}

@end
