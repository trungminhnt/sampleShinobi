#import "StockChartData.h"
#import "NSArray+StockChartUtils.h"

@interface StockChartData ()

@property (nonatomic, strong) NSMutableArray *ema12Array;
@property (nonatomic, strong) NSMutableArray *ema26Array;
@property (nonatomic, strong) NSMutableArray *ema9Array;
@property (nonatomic, strong) NSMutableArray *macdArray;

@end

@implementation StockChartData

#pragma mark - Shared Singleton

+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

- (NSInteger)periodTimeFromFreqTime:(FrequentInterval)interval
{
    NSUInteger second = 60;
    switch (interval) {
        case Freq1Min: second   *= 1; break;
        case Freq5Min: second   *= 5; break;
        case Freq15Min: second  *= 15; break;
        case Freq30Min: second  *= 30; break;
        case Freq1Hour: second  *= 60; break;
        case Freq6Hour: second  *= 6*60; break;
        case Freq12Hour: second *= 12*60; break;
        case Freq1D: second     *= 24*60; break;
        case Freq3D: second     *= 3*24*60; break;
        case Freq1W: second     *= 7*24*60; break;
        default: break;
    }
    return second;
}

- (void)loadChartDataFromBigJson:(NSDictionary *)dict period:(FrequentInterval)period onComplete:(void(^)())completeBlock
{
    // Setup OHLC data
    [self setupArrays];
    
    NSUInteger second = [self periodTimeFromFreqTime:period];
    NSString *keyInterval = [NSString stringWithFormat:@"%tu", second];
    NSArray *tickers = dict[keyInterval];
    
    NSInteger currentDataPoint = 0;
    for (int i = 0; i < tickers.count; i++) {
        NSString *object = tickers[i];
        NSArray *listItems = [object componentsSeparatedByString:@" "];
        
        // Date0 - Open1 - High2 - Low3 - Close4 - Volume5
        double time = [listItems[0] doubleValue] - second;
        NSDate *temp = [NSDate dateWithTimeIntervalSince1970:time];
        [self.dates addObject:temp];
        [self.volumes addObject:[NSNumber numberWithFloat:[listItems[5] floatValue]]];
        
        if (listItems.count == 6) {
            if (([listItems[1] floatValue] == 0) && ([listItems[2] floatValue] == 0) &&
                ([listItems[3] floatValue] == 0) && ([listItems[4] floatValue] == 0) && currentDataPoint >= 1) {
                // Get previous ticker
                [self.seriesOpen addObject:self.seriesClose[currentDataPoint - 1]];
                [self.seriesHigh addObject:self.seriesClose[currentDataPoint - 1]];
                [self.seriesLow addObject:self.seriesClose[currentDataPoint - 1]];
                [self.seriesClose addObject:self.seriesClose[currentDataPoint - 1]];
            } else {
                [self.seriesOpen addObject:[NSNumber numberWithFloat:[listItems[1] floatValue]]];
                [self.seriesHigh addObject:[NSNumber numberWithFloat:[listItems[2] floatValue]]];
                [self.seriesLow addObject:[NSNumber numberWithFloat:[listItems[3] floatValue]]];
                [self.seriesClose addObject:[NSNumber numberWithFloat:[listItems[4] floatValue]]];
            }
            
            /**
             *  Constructing a MACD is really quite simple, as soon as you know how to calculate moving averages.
             For any given stock or underlying security:
             1. Calculate a 12 day EMA of closing prices
             2. Calculate a 26 day EMA of closing prices
             3. Subtract the longer EMA in (2) from the shorter EMA in (1)
             4. Calculate a 9 day EMA of the MACD line gotten in (3)
             */
            [self createExponentialMovingAverage12Days:currentDataPoint withPeriod:StockChartEMA12Period];
            [self createExponentialMovingAverage26Days:currentDataPoint withPeriod:StockChartEMA26Period];
            
            // Calculate EMA 9 days of MACD for Signal line
            [self createExponentialMovingAverageOfMACD9Days:currentDataPoint withPeriod:StockChartMACDSignalLinePeriod];
            
            currentDataPoint++;
        }
    }
    
    _hasData = true;
    if (completeBlock) completeBlock();
}

- (void)setupArrays
{
//    self.seriesOpen  = nil;
//    self.seriesHigh  = nil;
//    self.seriesLow   = nil;
//    self.seriesClose = nil;
//    self.volumes     = nil;
//    self.dates       = nil;
//    self.ema12Array  = nil;
//    self.ema26Array  = nil;
//    self.ema9Array   = nil;
//    self.macdArray   = nil;
//    
//    self.seriesOpen  = [[NSMutableArray alloc] init];
//    self.seriesHigh  = [[NSMutableArray alloc] init];
//    self.seriesLow   = [[NSMutableArray alloc] init];
//    self.seriesClose = [[NSMutableArray alloc] init];
//    self.volumes     = [[NSMutableArray alloc] init];
//    self.dates       = [[NSMutableArray alloc] init];
//    self.ema12Array  = [[NSMutableArray alloc] init];
//    self.ema26Array  = [[NSMutableArray alloc] init];
//    self.ema9Array   = [[NSMutableArray alloc] init];
//    self.macdArray   = [[NSMutableArray alloc] init];
    
    [self.seriesOpen removeAllObjects];
    [self.seriesHigh removeAllObjects];
    [self.seriesLow removeAllObjects];
    [self.seriesClose removeAllObjects];
    [self.volumes removeAllObjects];
    [self.dates removeAllObjects];
    [self.ema12Array removeAllObjects];
    [self.ema26Array removeAllObjects];
    [self.ema9Array removeAllObjects];
    [self.macdArray removeAllObjects];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.seriesOpen  = [[NSMutableArray alloc] init];
        self.seriesHigh  = [[NSMutableArray alloc] init];
        self.seriesLow   = [[NSMutableArray alloc] init];
        self.seriesClose = [[NSMutableArray alloc] init];
        self.volumes     = [[NSMutableArray alloc] init];
        self.dates       = [[NSMutableArray alloc] init];
        self.ema12Array  = [[NSMutableArray alloc] init];
        self.ema26Array  = [[NSMutableArray alloc] init];
        self.ema9Array   = [[NSMutableArray alloc] init];
        self.macdArray   = [[NSMutableArray alloc] init];
        
        [self setupArrays];
    }
    return self;
}

- (NSNumber *)cleanUpCloseIfNeededWithHigh:(NSNumber*)high low:(NSNumber*)low close:(NSNumber*)originalClose
{
    float highValue = [high floatValue];
    float lowValue = [low floatValue];
    float closeValue = [originalClose floatValue];
    
    // If the closing value is outside of the high-low range, move it to midway in the range
    if (closeValue < lowValue || closeValue > highValue) {
        float range = highValue - lowValue;
        closeValue = lowValue + (range / 2);
    }
    return @(closeValue);
}

#pragma mark - MACD chart
/**
 *  Calculate Simple Moving Average
 *
 *  @param currentDataPoint Current index
 *  @param period           Period days
 *
 *  @return return SMA
 */
- (double)simpleMovingAverageAtCurrentPoint:(NSInteger)currentDataPoint withPeriod:(NSInteger)period inputArray:(NSArray *)input
{
    float sma = 0;
    for (NSInteger j = (currentDataPoint - period + 1); j <= currentDataPoint; j++) {
        sma += [input[j] doubleValue]; // Get close price
    }
    return sma / period;
}
/**
 *  Calculate EMA
 *
 *  @param sma              Simple Moving Average
 *  @param period           Period days
 *  @param currentDataPoint Current index
 *  @param input            Will calculate a period days EMA on this array
 *  @param output           Array contains previous EMA
 *
 *  @return return current EMA at currentDataPoint
 */
- (double)exponentialMovingAverageWithPeriod:(NSInteger)period
                         atDataPointIndex:(NSInteger)currentDataPoint
                               inputArray:(NSArray *)input
                              outputArray:(NSArray *)output
{
    double multiplier = (2.0 / (period + 1));
    double prevEMA    = [output[currentDataPoint - 1] doubleValue];
    double value      = [input[currentDataPoint] doubleValue];
    float ema         = (value - prevEMA) * multiplier + prevEMA;
    return ema;
}

- (double)exponentialMovingAverageWithSMA:(double)sma
                                   period:(NSInteger)period
                         atDataPointIndex:(NSInteger)currentDataPoint
                               inputArray:(NSArray *)input
                              outputArray:(NSArray *)output
{
    double multiplier = (2.0 / (period + 1));
    double prevEMA    = (output.count > 0) ? [output[currentDataPoint - 1] doubleValue] : sma;
    double value      = [input[currentDataPoint] doubleValue];
    float ema         = (value - prevEMA) * multiplier + prevEMA;
    return ema;
}

- (void)createExponentialMovingAverage12Days:(NSInteger)currentDataPoint withPeriod:(NSUInteger)periodEMA
{
    if (currentDataPoint >= periodEMA - 1) {
        if (currentDataPoint == periodEMA - 1) {
            // The first item of EMA array is SMA
            double sma = [self simpleMovingAverageAtCurrentPoint:currentDataPoint
                                                      withPeriod:periodEMA
                                                      inputArray:self.seriesClose];
            [self.ema12Array addObject:@(sma)];
        } else {
            // Calculate EMA 12 days
            double ema = [self exponentialMovingAverageWithPeriod:periodEMA
                                                 atDataPointIndex:currentDataPoint
                                                       inputArray:self.seriesClose
                                                      outputArray:self.ema12Array];
            [self.ema12Array addObject:@(ema)];
        }
    } else {
        [self.ema12Array addObject:@(0)];
    }
}

- (void)createExponentialMovingAverage26Days:(NSInteger)currentDataPoint withPeriod:(NSUInteger)periodEMA
{
    if (currentDataPoint >= periodEMA - 1) {
        if (currentDataPoint == periodEMA - 1) {
            // The first item of EMA array is SMA
            double sma = [self simpleMovingAverageAtCurrentPoint:currentDataPoint
                                                      withPeriod:periodEMA
                                                      inputArray:self.seriesClose];
            [self.ema26Array addObject:@(sma)];
        } else {
            // Calculate EMA 26 days
            double ema = [self exponentialMovingAverageWithPeriod:periodEMA
                                                 atDataPointIndex:currentDataPoint
                                                       inputArray:self.seriesClose
                                                      outputArray:self.ema26Array];
            [self.ema26Array addObject:@(ema)];
        }
    } else {
        [self.ema26Array addObject:@(0)];
    }
    
    [self.macdArray addObject:@([self.ema12Array[currentDataPoint] doubleValue] - [self.ema26Array[currentDataPoint] doubleValue])];
}

- (void)createExponentialMovingAverageOfMACD9Days:(NSInteger)currentDataPoint withPeriod:(NSUInteger)periodEMA
{
    if (currentDataPoint >= periodEMA - 1) {
        if (currentDataPoint == periodEMA - 1) {
            // The first item of EMA array is SMA
            double sma = [self simpleMovingAverageAtCurrentPoint:currentDataPoint
                                                      withPeriod:periodEMA
                                                      inputArray:self.macdArray];
            [self.ema9Array addObject:@(sma)];
        } else {
            // Calculate EMA 9 days
            double ema = [self exponentialMovingAverageWithPeriod:periodEMA
                                                 atDataPointIndex:currentDataPoint
                                                       inputArray:self.macdArray
                                                      outputArray:self.ema9Array];
            [self.ema9Array addObject:@(ema)];
        }
    } else {
        [self.ema9Array addObject:@(0)];
    }
}

/**
 *  Get y Value for MACD line
 */
- (NSNumber *)movingAverageConvergenceDivergenceAtIndex:(NSInteger)index
{
    if (index < 0 || index >= self.macdArray.count) return @(0);
    return self.macdArray[index];
}

/**
 *  Get y Value for Signal line of MACD line
 */
- (NSNumber *)signalLineMovingAverageConvergenceDivergenceAtIndex:(NSInteger)index
{
    // Get value at index of Signal Line with EMA-9
    if (index < 0 || index >= self.ema9Array.count) return @(0);
    return self.ema9Array[index];
}

- (NSNumber *)histogramMovingAverageConvergenceDivergenceAtIndex:(NSInteger)index
{
    // Get value at index of Signal Line with EMA-9
    if (index >= self.macdArray.count) return @(0);
    
    return @([self.macdArray[index] doubleValue] - [self.ema9Array[index] doubleValue]);
}

- (NSNumber *)yMinInRangeFromIndexOfMACDArray:(NSUInteger)startIdx toIndex:(NSUInteger)endIdx
{
    double minMACD   = [[self.macdArray minInRangeFromIndex:startIdx toIndex:endIdx] doubleValue];
    double minSignal = [[self.ema9Array minInRangeFromIndex:startIdx toIndex:endIdx] doubleValue];
    double histogram = minMACD - minSignal;
    
    __block float xMin = MAXFLOAT;
    [@[@(minMACD), @(minSignal), @(histogram)] enumerateObjectsUsingBlock:^(NSNumber *num, NSUInteger idx, BOOL *stop) {
        float x = num.floatValue;
        if (x < xMin) xMin = x;
    }];
    
    return @(xMin);
}

- (NSNumber *)yMaxInRangeFromIndexOfMACDArray:(NSUInteger)startIdx toIndex:(NSUInteger)endIdx
{
    double maxMACD = [[self.macdArray maxInRangeFromIndex:startIdx toIndex:endIdx] doubleValue];
    double maxSignal = [[self.ema9Array maxInRangeFromIndex:startIdx toIndex:endIdx] doubleValue];
    double histogram = maxMACD - maxSignal;
    
    __block float xMax = -MAXFLOAT;
    [@[@(maxMACD), @(maxSignal), @(histogram)] enumerateObjectsUsingBlock:^(NSNumber *num, NSUInteger idx, BOOL *stop) {
        float x = num.floatValue;
        if (x > xMax) xMax = x;
    }];
    
    return @(xMax);
}

#pragma mark - DataSource

- (NSUInteger)numberOfDataPoints
{
    return self.dates.count;
}

#pragma mark - Get Min/Max yAxis range CandleSticks chart

- (NSNumber *)yMinInRangeFromIndex:(NSUInteger)startIdx toIndex:(NSUInteger)endIdx
{
    return [self.seriesLow minInRangeFromIndex:startIdx
                                       toIndex:endIdx];
}

- (NSNumber *)yMaxInRangeFromIndex:(NSUInteger)startIdx toIndex:(NSUInteger)endIdx
{
    return [self.seriesHigh maxInRangeFromIndex:startIdx
                                        toIndex:endIdx];
}

#pragma mark - Reset

- (void)reset
{
    [self setupArrays];
    self.hasData = false;
}

#pragma mark - Dealloc

- (void)dealloc
{
    _seriesOpen  = nil;
    _seriesHigh  = nil;
    _seriesLow   = nil;
    _seriesClose = nil;
    _volumes = nil;
    _dates       = nil;
    _ema26Array  = nil;
    _ema9Array   = nil;
    _ema12Array  = nil;
    _macdArray   = nil;
}

@end
