//
//  CandleSticksChartDataSource.m
//  QuoineExiOS
//
//  Created by Trung Tran on 12/12/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import "CandleSticksChartDataSource.h"
#import "UIColor+TTAdditions.h"

@implementation CandleSticksChartDataSource

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

#pragma mark - Shared Singleton

+ (CandleSticksChartDataSource *)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[CandleSticksChartDataSource alloc] init];
    });
    return sharedInstance;
}

#pragma mark Datasource Protocol Functions

// Returns the number of series in the specified chart
- (NSInteger)numberOfSeriesInSChart:(ShinobiChart *)chart
{
    return 1;
}

// Returns the series at the specified index for a given chart
- (SChartSeries *)sChart:(ShinobiChart *)chart seriesAtIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            // Candlestick
            return [CandleSticksChartDataSource createCandlestickSeries];
        case 1:
            // Volume
            return [CandleSticksChartDataSource createColumnSeries];
        default:
            return nil;
    }
}

// Returns the number of points for a specific series in the specified chart
- (NSInteger)sChart:(ShinobiChart *)chart numberOfDataPointsForSeriesAtIndex:(NSInteger)seriesIndex
{
    return [self.data numberOfDataPoints];
}

- (SChartAxis*)sChart:(ShinobiChart *)chart yAxisForSeriesAtIndex:(NSInteger)index
{
    NSArray *allYAxes = [chart allYAxes];
    // The series at index 1 is our volume chart, which uses a different y axis. The other series use the default y axis
    if (index == 1) {
        return allYAxes[1];
    } else {
        return allYAxes[0];
    }
}

+ (SChartColumnSeries*)createColumnSeries
{
    SChartColumnSeries *columnSeries = [SChartColumnSeries new];
    columnSeries.style.areaColor = [UIColor chartBlueColor];
    columnSeries.style.showAreaWithGradient = NO;
    return columnSeries;
}

+ (SChartCandlestickSeries*)createCandlestickSeries
{
    // Create a candlestick series
    StockChartCandlestickSeries *candlestickSeries = [StockChartCandlestickSeries new];
    candlestickSeries.showFlatCandlesticks = true;
    // Define the data field names
    NSArray *keys = @[@"Open",@"High", @"Low", @"Close"];
    candlestickSeries.dataSeries.yValueKeys = keys;
    candlestickSeries.crosshairEnabled = YES;
    
    return candlestickSeries;
}

// Returns the data point at the specified index for the given series/chart.
- (id<SChartData>)sChart:(ShinobiChart *)chart dataPointAtIndex:(NSInteger)dataIndex forSeriesAtIndex:(NSInteger)seriesIndex
{
    switch (seriesIndex) {
        case 0:
            // Candlestick
            return [self candlestickDataPointAtIndex:dataIndex];
        case 1:
            // Volume
            return [self volumeDataPointAtIndex:dataIndex];
        default:
            return nil;
    }
}

- (NSArray *)sChart:(ShinobiChart *)chart dataPointsForSeriesAtIndex:(NSInteger)seriesIndex
{
    @try {
        NSMutableArray *datapoints = [NSMutableArray array];
        NSUInteger noPoints = [self sChart:chart numberOfDataPointsForSeriesAtIndex:seriesIndex];
        
        switch (seriesIndex) {
            case 0:
                // Candlestick
                for (int i=0; i<noPoints; i++) {
                    [datapoints addObject:[self candlestickDataPointAtIndex:i]];
                }
                break;
            case 1:
                // Volume
                for (int i=0; i<noPoints; i++) {
                    [datapoints addObject:[self volumeDataPointAtIndex:i]];
                }
                break;
            default:
                break;
        }
        
        if (datapoints.count == 0) {
            datapoints = nil;
        }
        
        return datapoints;
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

- (id<SChartData>)candlestickDataPointAtIndex:(NSUInteger)dataIndex
{
    @try {
        // Use a multi y datapoint
        SChartMultiYDataPoint *dp = [SChartMultiYDataPoint new];
        
        // Set the xValue (date)
        if (dataIndex < self.data.dates.count) {
            dp.xValue = self.data.dates[dataIndex];
        }
        
        // Get the open, high, low, close values
        float openVal  = 0, highVal  = 0, lowVal   = 0, closeVal = 0;
        @try {
            if (dataIndex < self.data.seriesOpen.count) {
                openVal  = [self.data.seriesOpen[dataIndex] floatValue];
                highVal  = [self.data.seriesHigh[dataIndex] floatValue];
                lowVal   = [self.data.seriesLow[dataIndex] floatValue];
                closeVal = [self.data.seriesClose[dataIndex] floatValue];
            }
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
        // Make sure all values are > 0
        openVal  = MAX(openVal, 0);
        highVal  = MAX(highVal, 0);
        lowVal   = MAX(lowVal, 0);
        closeVal = MAX(closeVal, 0);
        
        // Set the OHLC values
        NSDictionary *ohlcData = @{@"Open": @(openVal),
                                   @"High": @(highVal),
                                   @"Low": @(lowVal),
                                   @"Close": @(closeVal)};
        dp.yValues = [ohlcData mutableCopy];
        
        return dp;
    }
    @catch (NSException *exception) {
        SChartMultiYDataPoint *dp = [SChartMultiYDataPoint new];
        if (dataIndex < self.data.dates.count) {
            dp.xValue = self.data.dates[dataIndex];
        }
        // Set the OHLC values
        NSDictionary *ohlcData = @{@"Open": @(0),
                                   @"High": @(0),
                                   @"Low": @(0),
                                   @"Close": @(0)};
        dp.yValues = [ohlcData mutableCopy];
        return dp;
    }
    @finally {
    }
}

- (id<SChartData>)volumeDataPointAtIndex: (NSUInteger)dataIndex
{
    @try {
        SChartDataPoint *dp = [SChartDataPoint new];
        dp.xValue = self.data.dates[dataIndex];
        //    dp.yValue = self.chartData.volume[dataIndex];
        return dp;        
    }
    @catch (NSException *exception) {
        return nil;
    }
    @finally {
    }
}

#pragma mark - StockChartDatasourceLookup methods

- (id)estimateYValueForXValue:(id)xValue forSeriesAtIndex:(NSUInteger)idx
{
    [super estimateYValueForXValue:xValue forSeriesAtIndex:idx];
    NSUInteger index;
    @try {
        if ([xValue isKindOfClass:[NSNumber class]]) {
            // Need it to be a date since we are comparing timestamp
            xValue = [NSDate dateWithTimeIntervalSince1970:[xValue doubleValue]];
        }
        index = [self.data.dates indexOfBiggestObjectSmallerThan:xValue
                                                   inSortedRange:NSMakeRange(0, self.data.dates.count)];
    }
    @catch (NSException *exception) {
        index = 0;
    }
    
    @try {
        SChartDataPoint *dp = [self sChart:nil dataPointAtIndex:index forSeriesAtIndex:idx];
        if ([dp isKindOfClass:[SChartMultiYDataPoint class]]) {
            NSDictionary *yValues = ((SChartMultiYDataPoint*)dp).yValues;
            if (yValues[@"Close"]) {
                return yValues[@"Close"];
            } else {
                return yValues[@"High"];
            }
        } else {
            return dp.yValue;
        }
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

@end
