#import <Foundation/Foundation.h>
#import "TradingViewController.h"

static NSInteger const StockChartMovingAverageNPeriod = 20;
static NSInteger const StockChartEMA12Period = 12;
static NSInteger const StockChartEMA26Period = 26;
static NSInteger const StockChartMACDSignalLinePeriod = 9;

@interface StockChartData : NSObject

@property (nonatomic, strong) NSMutableArray *seriesOpen;
@property (nonatomic, strong) NSMutableArray *seriesHigh;
@property (nonatomic, strong) NSMutableArray *seriesLow;
@property (nonatomic, strong) NSMutableArray *seriesClose;
@property (nonatomic, strong) NSMutableArray *volumes;
@property (nonatomic, strong) NSMutableArray *dates;

#pragma mark - Data

+ (instancetype)sharedInstance;
- (void)loadChartDataFromBigJson:(NSDictionary *)dict period:(FrequentInterval)period onComplete:(void(^)())completeBlock;
- (NSUInteger)numberOfDataPoints;
@property (nonatomic) BOOL hasData;

#pragma mark - CandleSticks

- (NSNumber *)yMinInRangeFromIndex:(NSUInteger)startIdx toIndex:(NSUInteger)endIdx;
- (NSNumber *)yMaxInRangeFromIndex:(NSUInteger)startIdx toIndex:(NSUInteger)endIdx;

#pragma mark - MACD

- (NSNumber *)movingAverageConvergenceDivergenceAtIndex:(NSInteger)index;
- (NSNumber *)signalLineMovingAverageConvergenceDivergenceAtIndex:(NSInteger)index;
- (NSNumber *)histogramMovingAverageConvergenceDivergenceAtIndex:(NSInteger)index;

#pragma mark - Min/Max range
- (NSNumber *)yMinInRangeFromIndexOfMACDArray:(NSUInteger)startIdx toIndex:(NSUInteger)endIdx;
- (NSNumber *)yMaxInRangeFromIndexOfMACDArray:(NSUInteger)startIdx toIndex:(NSUInteger)endIdx;

- (void)reset;

@end
