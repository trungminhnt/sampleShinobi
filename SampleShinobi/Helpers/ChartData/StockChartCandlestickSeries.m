#import "StockChartCandlestickSeries.h"
#import "UIColor+TTAdditions.h"

@implementation StockChartCandlestickSeries

- (SChartCandlestickSeriesStyle*)styleForPoint:(id<SChartData>)point previousPoint:(id<SChartData>)prevPoint
{
    SChartCandlestickSeriesStyle *newStyle = [super styleForPoint:point previousPoint:prevPoint];

    // We color the candlestick based on this point's open and close values, and the previous
    // point's close value
    float open = [[point sChartYValueForKey: SChartCandlestickKeyOpen] floatValue];
    float close = [[point sChartYValueForKey: SChartCandlestickKeyClose] floatValue];
    float priorClose = [[prevPoint sChartYValueForKey: SChartCandlestickKeyClose] floatValue];

    newStyle.outlineWidth = @1.f;
    newStyle.stickWidth   = @1.f;
    
    // If today's closing price is higher than yesterday's, the candlestick should be green;
    // otherwise it should be red
    UIColor *candlestickColor = (close > priorClose) ? [UIColor chartGreenColor]
    : [UIColor chartRedColor];
    newStyle.outlineColor = candlestickColor;
    newStyle.stickColor = candlestickColor;
    
    // If the closing price is higher than the opening price, the candlestick should be hollow;
    // otherwise it should be filled with the outline/stick color
    if (close > open) {
        newStyle.risingColor = [UIColor clearColor];
        newStyle.risingColorGradient = [UIColor clearColor];
    } else {
        newStyle.fallingColor = candlestickColor;
        newStyle.fallingColorGradient = candlestickColor;
    }
    
    return newStyle;
}

@end
