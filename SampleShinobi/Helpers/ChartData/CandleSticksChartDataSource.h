//
//  CandleSticksChartDataSource.h
//  QuoineExiOS
//
//  Created by Trung Tran on 12/12/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseChartDataSource.h"

//NSString * const kCandleStickOpen  = @"Open";
//NSString * const kCandleStickHigh  = @"High";
//NSString * const kCandleStickLow   = @"Low";
//NSString * const kCandleStickClose = @"Close";

@interface CandleSticksChartDataSource : BaseChartDataSource

+ (CandleSticksChartDataSource *)sharedInstance;

@end
