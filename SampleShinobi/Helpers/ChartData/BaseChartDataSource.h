//
//  BaseChartDataSource.h
//  QuoineExiOS
//
//  Created by Trung Tran on 12/12/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ShinobiCharts/ShinobiCharts.h>
#import "StockChartData.h"
#import "StockChartDatasourceLookup.h"
#import "NSArray+StockChartUtils.h"
#import "StockChartCandlestickSeries.h"

@interface BaseChartDataSource : NSObject<SChartDatasource, StockChartDatasourceLookup>

@property (nonatomic, strong) StockChartData *data;

@end
