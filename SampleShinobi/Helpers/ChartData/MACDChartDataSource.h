//
//  MACDChartDataSource.h
//  QuoineExiOS
//
//  Created by Trung Tran on 12/9/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseChartDataSource.h"

@interface MACDChartDataSource : BaseChartDataSource

@property (nonatomic) double lastVisibleValue;

+ (MACDChartDataSource *)sharedInstance;

@end
