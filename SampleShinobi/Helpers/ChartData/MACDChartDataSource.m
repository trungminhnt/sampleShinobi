//
//  MACDChartDataSource.m
//  QuoineExiOS
//
//  Created by Trung Tran on 12/9/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import "MACDChartDataSource.h"
#import "UIColor+TTAdditions.h"

@implementation MACDChartDataSource

- (instancetype)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

#pragma mark - Shared Singleton

+ (MACDChartDataSource *)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MACDChartDataSource alloc] init];
    });
    return sharedInstance;
}

#pragma mark Datasource Protocol Functions

// Returns the number of series in the specified chart
- (NSInteger)numberOfSeriesInSChart:(ShinobiChart *)chart
{
    return 3;
}

- (SChartSeries *)sChart:(ShinobiChart *)chart seriesAtIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            return [MACDChartDataSource createMACDLineSerires];
        case 1:
            return [MACDChartDataSource createSignalLineSerires];
        case 2:
            return [MACDChartDataSource createHistogramColumnSeriesAtIndex:index];
        default:
            return nil;
    }
}

// Returns the number of points for a specific series in the specified chart
- (NSInteger)sChart:(ShinobiChart *)chart numberOfDataPointsForSeriesAtIndex:(NSInteger)seriesIndex
{
    return [self.data numberOfDataPoints];
}

- (SChartAxis*)sChart:(ShinobiChart *)chart yAxisForSeriesAtIndex:(NSInteger)index
{
    NSArray *allYAxes = [chart allYAxes];
    return allYAxes[0];
}

#pragma mark - Serires

+ (SChartLineSeries *)createMACDLineSerires
{
    // Create a Band series
    SChartLineSeries *lineSeries = [SChartLineSeries new];
    lineSeries.style.lineWidth = [NSNumber numberWithFloat:1.0f];
    lineSeries.crosshairEnabled = false;
    lineSeries.style.lineColor = [UIColor chartMACDLineColor];
    lineSeries.title = @"MACD line";
    return lineSeries;
}

+ (SChartLineSeries *)createSignalLineSerires
{
    // Create a Band series
    SChartLineSeries *lineSeries = [SChartLineSeries new];
    lineSeries.style.lineWidth = [NSNumber numberWithFloat:1.0f];
    lineSeries.crosshairEnabled = false;
    lineSeries.style.lineColor = [UIColor chartSignalLineColor];
    lineSeries.title = @"Signal line";
    return lineSeries;
}

+ (SChartColumnSeries*)createHistogramColumnSeriesAtIndex:(NSUInteger)index
{
    SChartColumnSeries *columnSeries = [SChartColumnSeries new];
    columnSeries.style.areaColor = [UIColor chartGreenColor];
    columnSeries.style.areaColorBelowBaseline = [[UIColor chartGreenColor] colorWithAlphaComponent:0.3];
    columnSeries.style.showAreaWithGradient = NO;
    return columnSeries;
}

#pragma mark - Get Data Point

// Returns the data point at the specified index for the given series/chart.
- (id<SChartData>)sChart:(ShinobiChart *)chart dataPointAtIndex:(NSInteger)dataIndex forSeriesAtIndex:(NSInteger)seriesIndex
{
    switch (seriesIndex) {
        case 0:
            return [self dataPointAtIndexForMACDLine:dataIndex];
        case 1:
            return [self dataPointAtIndexForSignalLine:dataIndex];
        case 2:
            return [self dataPointAtIndexForHistogram:dataIndex];
        default:
            return nil;
    }
}

- (id<SChartData>)dataPointAtIndexForMACDLine:(NSUInteger)dataIndex
{
    @try {
        // Construct a data point to return
        SChartMultiYDataPoint *datapoint = [SChartMultiYDataPoint new];
        if (dataIndex < self.data.dates.count) {
            datapoint.xValue = self.data.dates[dataIndex];
        }
        
        datapoint.yValue = [self.data movingAverageConvergenceDivergenceAtIndex:dataIndex];
        return datapoint;
    }
    @catch (NSException *exception) {
        return nil;
    }
    @finally {
    }
}

- (id<SChartData>)dataPointAtIndexForSignalLine:(NSUInteger)dataIndex
{
    @try {
        // Construct a data point to return
        SChartMultiYDataPoint *datapoint = [SChartMultiYDataPoint new];
        if (dataIndex < self.data.dates.count) {
            datapoint.xValue = self.data.dates[dataIndex];
        }
        
        datapoint.yValue = [self.data signalLineMovingAverageConvergenceDivergenceAtIndex:dataIndex];
        return datapoint;
    }
    @catch (NSException *exception) {
        return nil;
    }
    @finally {
    }
}

- (id<SChartData>)dataPointAtIndexForHistogram:(NSUInteger)dataIndex
{
    @try {
        SChartDataPoint *dp = [SChartDataPoint new];
        if (dataIndex < self.data.dates.count) {
            dp.xValue = self.data.dates[dataIndex];
        }
        
        dp.yValue = [self.data histogramMovingAverageConvergenceDivergenceAtIndex:dataIndex];
        return dp;
    }
    @catch (NSException *exception) {
        return nil;
    }
    @finally {
    }
}

#pragma mark - StockChartDatasourceLookup methods

- (id)estimateYValueForXValue:(id)xValue forSeriesAtIndex:(NSUInteger)idx
{
    [super estimateYValueForXValue:xValue forSeriesAtIndex:idx];
    NSUInteger index;
    @try {
        if ([xValue isKindOfClass:[NSNumber class]]) {
            // Need it to be a date since we are comparing timestamp
            xValue = [NSDate dateWithTimeIntervalSince1970:[xValue doubleValue]];
        }
        index = [self.data.dates indexOfBiggestObjectSmallerThan:xValue
                                                   inSortedRange:NSMakeRange(0, self.data.dates.count)];
    }
    @catch (NSException *exception) {
        index = 0;
    }
    
    @try {
        SChartDataPoint *dp = [self sChart:nil dataPointAtIndex:index forSeriesAtIndex:idx];
        return dp.yValue;
    }
    @catch (NSException *exception) {
    }
}

@end
