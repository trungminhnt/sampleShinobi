//
//  NSDictionary+TTAdditions.m
//  QuoineExchange
//
//  Created by Trung Tran on 10/22/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import "NSDictionary+TTAdditions.h"

@implementation NSDictionary (TTAdditions)

- (NSDictionary *)dictionaryByReplacingNullsWithStrings
{
    const NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary:self];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    
    for (NSString *key in self) {
        const id object = [self objectForKey: key];
        if (object == nul) {
            [replaced setObject: blank forKey: key];
        }
        else if ([object isKindOfClass: [NSDictionary class]]) {
            [replaced setObject: [(NSDictionary *) object dictionaryByReplacingNullsWithStrings] forKey: key];
        }
    }
    return [NSDictionary dictionaryWithDictionary:(NSDictionary*)replaced];
}

@end
