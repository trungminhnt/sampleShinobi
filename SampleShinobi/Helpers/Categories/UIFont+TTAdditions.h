//
//  UIFont+TTAdditions.h
//  QuoineExiOS
//
//  Created by Trung Tran on 12/8/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (TTAdditions)

+ (UIFont *)regularRobotoCondensedOfSize:(CGFloat)fontSize;
+ (UIFont *)lightSFUIDisplayOfSize:(CGFloat)fontSize;
+ (UIFont *)regularSFUITextOfSize:(CGFloat)fontSize;
+ (UIFont *)mediumSFUITextOfSize:(CGFloat)fontSize;
+ (UIFont *)semiBoldSFUITextOfSize:(CGFloat)fontSize;

@end
