//
//  NSDictionary+TTAdditions.h
//  QuoineExchange
//
//  Created by Trung Tran on 10/22/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (TTAdditions)

- (NSDictionary *)dictionaryByReplacingNullsWithStrings;

@end
