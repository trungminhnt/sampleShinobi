//
//  UIColor+TTAdditions.m
//  QuoineExiOS
//
//  Created by Trung Tran on 12/7/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import "UIColor+TTAdditions.h"
#import "UIColor+BTUtils.h"

@implementation UIColor (TTAdditions)

#pragma mark - Chart

+ (UIColor *)chartRedColor
{
    return [UIColor colorWithRed:1.00 green:0.00 blue:0.00 alpha:1.0];
}

+ (UIColor *)chartGreenColor
{
    return [UIColor colorWithRed:0.00 green:0.67 blue:0.00 alpha:1.0];
}

+ (UIColor *)chartDarkGrayColor
{
    return [UIColor colorWithRed:83.0/255 green:96.0/255 blue:107.0/255 alpha:1];
}

+ (UIColor *)chartPurpleColor
{
    // Bollinger bands color
    return [UIColor colorWithRed:113.0/255 green:32.0/255 blue:123.0/255 alpha:1];
}

+ (UIColor *)chartBlueColor
{
    return [UIColor colorWithRed:0.17 green:0.67 blue:0.95 alpha:1];
}

+ (UIColor *)chartSignalLineColor
{
    return [UIColor colorWithHexString:@"#9013FE"];
}

+ (UIColor *)chartMACDLineColor
{
    return [UIColor colorWithHexString:@"#50E3C2"];
}

+ (UIColor *)chartBackgroundColor
{
    return [UIColor whiteColor];
}

#pragma mark - Text color

+ (UIColor *)textAfterDecimalPointColor
{
    return [UIColor colorWithRed:0.39 green:0.39 blue:0.39 alpha:1];
}

+ (UIColor *)textBeforeDecimalPointColor
{
    return [UIColor colorWithRed:0.62 green:0.62 blue:0.62 alpha:1];
}

#pragma mark - NavigationBar color

+ (UIColor *)navigationBarColor
{
    return [UIColor colorWithHexString:@"F0F0F0"];
}

@end
