//
//  UIColor+TTAdditions.h
//  QuoineExiOS
//
//  Created by Trung Tran on 12/7/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (TTAdditions)

// Chart
+ (UIColor *)chartRedColor;
+ (UIColor *)chartGreenColor;
+ (UIColor *)chartDarkGrayColor;
+ (UIColor *)chartPurpleColor;
+ (UIColor *)chartBlueColor;
+ (UIColor *)chartSignalLineColor;
+ (UIColor *)chartMACDLineColor;
+ (UIColor *)chartBackgroundColor;

// Text
+ (UIColor *)textAfterDecimalPointColor;
+ (UIColor *)textBeforeDecimalPointColor;

+ (UIColor *)navigationBarColor;

@end
