//
//  UIFont+TTAdditions.m
//  QuoineExiOS
//
//  Created by Trung Tran on 12/8/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import "UIFont+TTAdditions.h"

@implementation UIFont (TTAdditions)

+ (UIFont *)regularRobotoCondensedOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"RobotoCondensed-Regular" size:fontSize];
}

+ (UIFont *)lightSFUIDisplayOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"SFUIDisplay-Light" size:fontSize];
}

+ (UIFont *)regularSFUITextOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"SFUIText-Regular" size:fontSize];
}

+ (UIFont *)mediumSFUITextOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"SFUIText-Medium" size:fontSize];
}

+ (UIFont *)semiBoldSFUITextOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"SFUIText-Semibold" size:fontSize];
}


@end
