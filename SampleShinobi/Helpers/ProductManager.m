//
//  ProductManager.m
//  QuoineExiOS
//
//  Created by Trung Tran on 12/15/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import "ProductManager.h"

@interface ProductManager ()

@end

@implementation ProductManager

- (id)init
{
    if (self = [super init]) {
        self.periodTime = Freq1Hour;
    }
    return self;
}

#pragma mark - Shared Singleton

+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

- (void)dealloc
{
    // implement -dealloc & remove abort() when refactoring for
    // non-singleton use.
    abort();
}
 
@end
