#import "StockChartValueView.h"
#import "StockChartArrowHead.h"

@implementation StockChartValueView

- (instancetype)initWithText:(NSString *)text andFont:(UIFont *)font
               withTextColor:(UIColor *)textColor withBackgroundColor:(UIColor *)bgColor
{
    self = [super init];
    if (self) {
        // Add the label
        self.label = [[UILabel alloc] initWithFrame:self.bounds];
        self.label.backgroundColor = bgColor;
        self.label.font            = font;
        self.label.textColor       = textColor;
        self.label.text            = text;
        self.label.textAlignment   = NSTextAlignmentCenter;
        self.label.adjustsFontSizeToFitWidth = true;
        [self.label sizeToFit];
        
        // Add the arrow
        StockChartArrowHead *arrowHead = [[StockChartArrowHead alloc] initWithFrame:CGRectMake(0,
                                                                                               0,
                                                                                               0,
                                                                                               self.label.bounds.size.height)
                                                                              color:bgColor];
        
        self.label.center = CGPointMake(arrowHead.bounds.size.width + self.label.center.x - 0.5, self.label.center.y);
        [self addSubview:self.label];
        [self addSubview:arrowHead];
        
        // Fit the frame to the contents
        self.frame = CGRectMake(0,
                                0,
                                arrowHead.bounds.size.width + self.label.bounds.size.width,
                                self.label.bounds.size.height);
    }
    return self;
}

- (void)setPosition:(CGPoint)leftMiddlePosition
{
    self.center = CGPointMake(leftMiddlePosition.x + self.bounds.size.width / 2,
                              leftMiddlePosition.y);
}

- (void)setTextColor:(UIColor *)textColor
{
    if (!textColor) return;
    self.label.textColor = textColor;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    if (!backgroundColor) return;
    self.label.backgroundColor = backgroundColor;
}

- (void)setFont:(UIFont *)font
{
    if (!font) return;
    self.label.font = font;
}

#pragma mark - Dealloc

- (void)dealloc
{
    _label = nil;
}

@end
