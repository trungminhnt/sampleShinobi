#import "StockChartArrowHead.h"

@interface StockChartArrowHead ()

@property (strong, nonatomic) CAShapeLayer *arrowHead;

@end

@implementation StockChartArrowHead

- (instancetype)initWithFrame:(CGRect)frame color:(UIColor*)color
{
    self = [super initWithFrame:frame];
    if (self) {
        self.color = color;
        self.backgroundColor = [UIColor clearColor];
        
        self.arrowHead = [CAShapeLayer layer];
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:CGPointMake(frame.size.width, 0)];
        [path addLineToPoint:CGPointMake(0, frame.size.height)];
        [path addLineToPoint:CGPointMake(frame.size.width, frame.size.height)];
        [path addLineToPoint:CGPointMake(frame.size.width, 0)];
        self.arrowHead.path = path.CGPath;
        self.arrowHead.lineWidth = 0;
        self.arrowHead.anchorPoint = CGPointMake(0, 0.5);
        self.arrowHead.fillColor = self.color.CGColor;
        
        [self.layer addSublayer:self.arrowHead];
    }
    return self;
}

- (void)setColor:(UIColor *)color
{
    _color = color;
    self.arrowHead.fillColor = color.CGColor;
}

- (void)dealloc
{
    _color = nil;
}

@end
