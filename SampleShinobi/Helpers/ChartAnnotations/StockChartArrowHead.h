#import <UIKit/UIKit.h>

@interface StockChartArrowHead : UIView

@property (nonatomic, strong) UIColor *color;

- (instancetype)initWithFrame:(CGRect)frame color:(UIColor*)color;

@end
