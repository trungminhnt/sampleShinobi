#import <ShinobiCharts/ShinobiCharts.h>

@interface StockChartValueView : UIView

@property (strong, nonatomic) UILabel *label;

@property (nonatomic, assign) UIColor *textColor;
@property (nonatomic, assign) UIColor *backgroundColor;
@property (nonatomic, assign) UIFont *font;

- (instancetype)initWithText:(NSString *)text andFont:(UIFont *)font
               withTextColor:(UIColor *)textColor withBackgroundColor:(UIColor *)bgColor;

- (void)setPosition:(CGPoint)leftMiddlePosition;

@end
