#import <Foundation/Foundation.h>
#import <ShinobiCharts/ShinobiChart.h>
#import "StockChartDatasourceLookup.h"

/**
 Class to add an annotation to the chart with a dashed line and label showing the value at
 the last point displayed.
 To use this class, you should call updateValueAnnotationForXAxisRangeLyAxisRange: whenever
 the chart's range changes.
 There's a detailed tutorial on how to create a similar value annotation at:
 http://www.shinobicontrols.com/blog/posts/2013/05/building-a-range-selector-with-shinobicharts-part-iv
 */
@interface StockChartValueAnnotationManager : NSObject

@property (nonatomic, assign) bool disableLine;
@property (nonatomic, strong) UIColor *textColor;

- (instancetype)initWithChart:(ShinobiChart *)chart
                   datasource:(id<StockChartDatasourceLookup>)datasource
                  seriesIndex:(NSInteger)seriesIndex
                 rightPadding:(CGFloat)rightPadding;

// Updates the value annotation based on the given ranges, and redraws the chart
- (void)updateValueAnnotationForXAxisRange:(SChartRange *)xRange yAxisRange:(SChartRange *)yRange;

// Updates the value annotation based on the given ranges, with optional redraw
- (void)updateValueAnnotationForXAxisRange:(SChartRange *)xRange yAxisRange:(SChartRange *)yRange
                                    redraw:(BOOL)redraw;

@end
