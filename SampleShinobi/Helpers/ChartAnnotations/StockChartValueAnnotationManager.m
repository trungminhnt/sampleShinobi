#import "StockChartValueAnnotationManager.h"
#import "StockChartValueView.h"
#import "StockChartDashedLineAnnotation.h"
#import <ShinobiCharts/SChartCanvas.h>
#import <ShinobiCharts/SChartGLView.h>
#import "MACDChartDataSource.h"
#import "UIColor+TTAdditions.h"
#import "UIFont+TTAdditions.h"
#define kReloadMACDLastVisible   @"ReloadMACDLastVisible"

@interface StockChartValueAnnotationManager ()

@property (nonatomic, strong) ShinobiChart *chart;
@property (nonatomic, strong) id<StockChartDatasourceLookup> datasource;
@property (nonatomic, assign) NSInteger seriesIndex;
@property (nonatomic, strong) SChartAnnotation *lineAnnotation;
@property (nonatomic, strong) StockChartValueView *valueView;
@property (nonatomic, assign) CGFloat rightPadding;

@end

@implementation StockChartValueAnnotationManager

- (instancetype)init
{
    NSException *exception = [NSException exceptionWithName:NSInvalidArgumentException
                                                     reason:@"Please use initWithChart:seriesIndex:" userInfo:nil];
    @throw exception;
}

#pragma mark - Accessory

- (void)setDisableLine:(bool)disableLine
{
    if (!disableLine) return;
    _disableLine = disableLine;
    [self.chart removeAnnotation:self.lineAnnotation];
}

- (void)setTextColor:(UIColor *)textColor
{
    if (!textColor) return;
    _textColor = textColor;
    self.valueView.textColor = textColor;
}

#pragma mark - 

- (instancetype)initWithChart:(ShinobiChart *)chart
                   datasource:(id<StockChartDatasourceLookup>)datasource
                  seriesIndex:(NSInteger)seriesIndex
                 rightPadding:(CGFloat)rightPadding
{
    self = [super init];
    if (self) {
        self.rightPadding = rightPadding;
        self.chart = chart;
        self.seriesIndex = seriesIndex;
        self.datasource = datasource;
        self.textColor = [UIColor chartRedColor];
        [self createLine];
        [self createText];
    }
    return self;
}

- (void)createLine
{
    self.lineAnnotation = [[StockChartDashedLineAnnotation alloc] initWithYValue:nil
                                                                           xAxis:self.chart.xAxis
                                                                           yAxis:self.chart.yAxis
                                                                    rightPadding:self.rightPadding];
    [self.chart addAnnotation:self.lineAnnotation];
}

- (void)createText
{
    // Create our text annotation subclass. We set the text to be the widest of our possible values
    // since we only size the annotation at construction time.
    self.valueView = [[StockChartValueView alloc] initWithText:@"MM.MM"
                                                       andFont:[UIFont regularSFUITextOfSize:11]
                                                 withTextColor:self.textColor
                                           withBackgroundColor:[UIColor clearColor]];
    [self.chart addSubview:self.valueView];
}

#pragma mark - API Methods

- (void)updateValueAnnotationForXAxisRange:(SChartRange *)xRange yAxisRange:(SChartRange *)yRange
                                    redraw:(BOOL)redraw
{
    // Need to find the y-value at the maximum of the given x-value range
    id lastVisibleDPValue = [self.datasource estimateYValueForXValue:xRange.maximum
                                                    forSeriesAtIndex:self.seriesIndex];
    CGFloat lastVisibleDPValueDouble = [lastVisibleDPValue doubleValue];
    if ([self.datasource isKindOfClass:[MACDChartDataSource class]] && self.seriesIndex == 0) {
        [MACDChartDataSource sharedInstance].lastVisibleValue = lastVisibleDPValueDouble;
        [[NSNotificationCenter defaultCenter] postNotificationName:kReloadMACDLastVisible object:nil];
    }
    if ([lastVisibleDPValue compare:yRange.minimum] == NSOrderedAscending ||
        [lastVisibleDPValue compare:yRange.maximum] == NSOrderedDescending) {
        self.lineAnnotation.alpha = 0;
        self.valueView.alpha = 0;
    } else {
        // Update the value on line annotation
        self.lineAnnotation.yValue = lastVisibleDPValue;
        [self.lineAnnotation updateViewWithCanvas:self.chart.canvas];
        
        // Update position and text of value view
        CGPoint pointInPlotArea = CGPointMake(CGRectGetMaxX([self.chart getPlotAreaFrame]),
                                              [self.chart.yAxis pixelValueForDataValue:lastVisibleDPValue]);
        CGPoint pointInChart = [self.chart convertPoint:pointInPlotArea
                                               fromView:self.chart.canvas.glView];
        [self.valueView setPosition:pointInChart];
        
        NSUInteger precision = 2;
        NSString *format = [[@"%." stringByAppendingFormat:@"%tu", precision] stringByAppendingString:@"f"];
        NSString *priceStr = [NSString stringWithFormat:format, lastVisibleDPValueDouble];
        self.valueView.label.text = priceStr;
        
        // Make sure they're both visible
        self.lineAnnotation.alpha = 1;
        self.valueView.alpha = 1;
    }
    
    if (redraw) {
        [self.chart redrawChart];
    }
}

- (void)updateValueAnnotationForXAxisRange:(SChartRange *)xRange yAxisRange:(SChartRange *)yRange
{
    [self updateValueAnnotationForXAxisRange:xRange yAxisRange:yRange redraw:YES];
}

@end
