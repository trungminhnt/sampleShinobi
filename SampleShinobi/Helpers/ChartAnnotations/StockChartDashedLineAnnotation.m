#import "StockChartDashedLineAnnotation.h"
#import "UIColor+TTAdditions.h"

@implementation StockChartDashedLineAnnotation

- (instancetype)initWithYValue:(id)yValue xAxis:(SChartAxis *)xAxis yAxis:(SChartAxis*)yAxis rightPadding:(CGFloat)rightPadding
{
    // Calculate the annotation width based on the xAxis range
//    CGFloat width = [xAxis pixelValueForDataValue:xAxis.range.maximum];
    CGFloat width = [[UIScreen mainScreen] bounds].size.width - [yAxis.width floatValue] - rightPadding;
    self = [super initWithFrame:CGRectMake(0, 0, width, 1)];
    if (self) {
        self.xAxis = xAxis;
        self.yAxis = yAxis;
        self.yValue = yValue;
        self.xValue = nil;
        self.backgroundColor = [UIColor clearColor];
        
        UIBezierPath *dashedLinePath = [UIBezierPath bezierPath];
        CGPoint leftPoint = CGPointMake(0.0, 0.0);
        CGPoint rightPoint = CGPointMake(self.frame.size.width, 0.0);
        [dashedLinePath moveToPoint:leftPoint];
        [dashedLinePath addLineToPoint:rightPoint];
        
        CAShapeLayer *dashedLineLayer = [CAShapeLayer layer];
        dashedLineLayer.path = dashedLinePath.CGPath;
        dashedLineLayer.strokeColor = [UIColor chartRedColor].CGColor;
        dashedLineLayer.lineDashPattern = @[@1, @1];
        dashedLineLayer.lineWidth = 1;
        [self.layer addSublayer:dashedLineLayer];
    }
    return self;
}

@end
