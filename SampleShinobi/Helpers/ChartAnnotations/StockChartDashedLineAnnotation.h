#import <UIKit/UIKit.h>
#import <ShinobiCharts/ShinobiCharts.h>

@interface StockChartDashedLineAnnotation : SChartAnnotation

- (instancetype)initWithYValue:(id)yValue xAxis:(SChartAxis *)xAxis yAxis:(SChartAxis*)yAxis rightPadding:(CGFloat)rightPadding;

@end
