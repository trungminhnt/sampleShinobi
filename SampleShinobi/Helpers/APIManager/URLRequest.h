//
//  URLRequest.h
//
//  Created by Trung Tran on 10/5/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface URLRequest : NSObject

+ (void)shareRequestWithUrl:(NSString*)url;

+ (AFHTTPRequestOperation*)getWithEndpoint:(NSString*)endpoint
                                parameters:(NSDictionary *)parameters
                                   success:(void ( ^ ) ( AFHTTPRequestOperation *operation , id responseObject ))success
                                   failure:(void ( ^ ) ( AFHTTPRequestOperation *operation , NSError *error ))failure;

@end
