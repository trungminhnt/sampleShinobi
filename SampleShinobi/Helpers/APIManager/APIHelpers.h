//
//  APIHelpers.h
//  ATViOSApp
//
//  Created by Trung Tran on 10/5/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIHelpers : NSObject

#pragma mark - Chart

+ (void)getTickersWithProductCode:(NSString *)code
                       onComplete:(void (^)(NSDictionary *dict))completeBlock
                        onFailure:(void (^)(NSString *message))failureBlock;

@end
