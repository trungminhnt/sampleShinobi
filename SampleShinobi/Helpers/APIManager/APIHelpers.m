//
//  APIHelpers.m
//  ATViOSApp
//
//  Created by Trung Tran on 10/5/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import "APIHelpers.h"
#import "URLRequest.h"

@implementation APIHelpers

#pragma mark - Errors

+ (NSString*)buildEndPoint:(NSString*)endPoint
{
    return endPoint;
}

+ (NSString*)errorMessageFromResponse:(id)response
{
    if ([response isKindOfClass:[NSArray class]]) {
        return nil;
    }
    
    if ([response isKindOfClass:[NSDictionary class]]) {
        if (response[@"error"] && response[@"error"][@"message"]) {
            return response[@"error"][@"message"];
        }
    } else {
        return @"Wrong data structure";
    }
    return nil;
}

+ (NSString*)parseErrorMessageWithResponse:(NSDictionary*)json error:(NSError*)error
{
    if (![json isKindOfClass:[NSDictionary class]]) {
        if (json == nil) {
            if (error) {
                return error.userInfo[@"NSLocalizedDescription"];
            }
            return @"Unknown error";
        }
        return [NSString stringWithFormat:@"%@", json];
    }
    if (json[@"message"]) return json[@"message"];
    if (json[@"errors"]) {
        id responseError = json[@"errors"];
        if ([responseError isKindOfClass:[NSString class]]) {
            return responseError;
        }
        if ([responseError isKindOfClass:[NSArray class]]) {
            NSArray * arrError = (NSArray*)responseError;
            if (arrError.count) return arrError[0];
        }
        if ([responseError isKindOfClass:[NSDictionary class]]) {
            NSDictionary * dicError = (NSDictionary*)responseError;
            if (dicError.count) {
                id messageObject = dicError[dicError.allKeys[0]];
                if ([messageObject isKindOfClass:[NSArray class]] && ((NSArray*)messageObject).count) {
                    messageObject = messageObject[0];
                }
                if ([messageObject isKindOfClass:[NSString class]]) {
                    return [NSString stringWithFormat:@"%@_%@", dicError.allKeys[0], messageObject];
                }
            };
        }
    }
    return @"Unknown error";
}

#pragma mark - Chart

+ (void)getTickersWithProductCode:(NSString *)code
                       onComplete:(void (^)(NSDictionary *dict))completeBlock
                        onFailure:(void (^)(NSString *message))failureBlock
{
    NSString * endPoint = [NSString stringWithFormat:@"http://cryptowat.ch/quoine/%@", [code lowercaseString]];
    [URLRequest getWithEndpoint:endPoint
                     parameters:nil
                        success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString * message = [self errorMessageFromResponse:responseObject];
         if (message) {
             if (failureBlock) failureBlock (message);
         } else {
             if (completeBlock) completeBlock (responseObject);
         }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         if (failureBlock) failureBlock ([self parseErrorMessageWithResponse:operation.responseObject error:error]);
     }];
}

@end
