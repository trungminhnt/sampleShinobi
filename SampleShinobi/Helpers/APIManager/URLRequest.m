//
//  URLRequest.m
//
//  Created by Trung Tran on 10/5/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import "URLRequest.h"

@implementation URLRequest

static NSString * _shareUrl;

+ (void)shareRequestWithUrl:(NSString*)urlStr
{
    _shareUrl = urlStr;
}

+ (AFHTTPRequestOperation*)getWithEndpoint:(NSString*)endpoint
                                parameters:(NSDictionary *)parameters
                                   success:(void ( ^ ) ( AFHTTPRequestOperation *operation , id responseObject ))success
                                   failure:(void ( ^ ) ( AFHTTPRequestOperation *operation , NSError *error ))failure
{
    AFHTTPRequestOperationManager * manager = [self requestOperationManagerWithEndPoint:endpoint];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    return [manager GET:endpoint
             parameters:parameters
                success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    if(success) success(operation, responseObject);
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                    if(failure) failure(operation, error);
                }];
}

#pragma mark - Helper

+ (AFHTTPRequestOperationManager*)requestOperationManagerWithEndPoint:(NSString *)endPoint
{
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:_shareUrl]];
    manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    manager.requestSerializer = serializer;
    return manager;
}

@end
