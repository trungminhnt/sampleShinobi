//
//  Constants.m
//  ATViOSApp
//
//  Created by Trung Tran on 10/5/15.
//  Copyright © 2015 Trung Tran. All rights reserved.
//

#import "Constants.h"

@implementation Constants

#pragma mark - ShinobiControls

+ (NSString *)shinobiControlsKey
{
    // 12 March, 2016
    return
    @"Sg8XBPRE4Eq990sMjAxNjA1MTBzZTdlbl9sb3ZlNjc4OUB5YWhvby5jb20=isCfFSZaZZ8qi6Jt1aUh14cnacNBQSrfTj9e/Xz3osWVWEHT8xKQQhVoFAfvN4AxH0W3v+IkBJYD/G7Sb6ine8nx3UVNzWVF82fpqU1Umk3b4kaBmtira7EjxNuSMVPneIQNkuFep2zm6h/YruAfhv/HAteI=AXR/y+mxbZFM+Bz4HYAHkrZ/ekxdI/4Aa6DClSrE4o73czce7pcia/eHXffSfX9gssIRwBWEPX9e+kKts4mY6zZWsReM+aaVF0BL6G9Vj2249wYEThll6JQdqaKda41AwAbZXwcssavcgnaHc3rxWNBjJDOk6Cd78fr/LwdW8q7gmlj4risUXPJV0h7d21jO1gzaaFCPlp5G8l05UUe2qe7rKbarpjoddMoXrpErC9j8Lm5Oj7XKbmciqAKap+71+9DGNE2sBC+sY4V/arvEthfhk52vzLe3kmSOsvg5q+DQG/W9WbgZTmlMdWHY2B2nbgm3yZB7jFCiXH/KfzyE1A==PFJTQUtleVZhbHVlPjxNb2R1bHVzPnh6YlRrc2dYWWJvQUh5VGR6dkNzQXUrUVAxQnM5b2VrZUxxZVdacnRFbUx3OHZlWStBK3pteXg4NGpJbFkzT2hGdlNYbHZDSjlKVGZQTTF4S2ZweWZBVXBGeXgxRnVBMThOcDNETUxXR1JJbTJ6WXA3a1YyMEdYZGU3RnJyTHZjdGhIbW1BZ21PTTdwMFBsNWlSKzNVMDg5M1N4b2hCZlJ5RHdEeE9vdDNlMD08L01vZHVsdXM+PEV4cG9uZW50PkFRQUI8L0V4cG9uZW50PjwvUlNBS2V5VmFsdWU+";
}

@end
