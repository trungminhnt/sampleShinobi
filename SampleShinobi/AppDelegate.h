//
//  AppDelegate.h
//  SampleShinobi
//
//  Created by Trung Tran on 4/20/16.
//  Copyright © 2016 Tasumi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

